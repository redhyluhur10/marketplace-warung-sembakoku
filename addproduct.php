<?php
include "functions.php";
if (isset($_POST["submit"])){


if(tambah($_POST)>0){
    echo"<script>
        alert('insert data success')
        document.location.href = 'addproduct.php'
        </script>
    ";
}else{
    echo "<script>
    alert('insert data failed')
    document.location.href = 'addproduct.php'
    </script>";
}

}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>INPUT PRODUCT</title>
    <link rel="stylesheet" href="addproduct.css">
    <link rel="stylesheet" href="dashboard.css">
</head>

<body>
    <main>
        <input type="checkbox" id="check">
        <label for="check">
            <i class="fas fa-bars" id="btn"></i>
            <i class="fa fa-arrow-right" id="open"></i>
        </label>
        <div class="sidebar">
            <div class="top">
                Dashboard
            </div>  
            <ul>
                <li><a class="#" href="profile.html"><i class="fa fa-home"></i> Profile</a></li>
                <li><a class="#" href="addproduct.php"><i class="fa fa-shopping-basket"></i> Input Product</a></li>
                <li><a class="#" href="stockproduct.php"><i class="fa fa-shopping-bag"></i> Stock Products</a></li>
                <li><a class="#" href="index.html"><i class="fa fa-user-circle"></i> LogOut</a></li>
            </ul>
        </div>
    </main>
    <div class="wrapper">
        <h2>INPUT PRODUCT</h2>

        <form action="" method="post" enctype="multipart/form-data">
            <div class="form-group">
                <label for="">Product Name</label>
                <input type="text" name="nama">
            </div>
            <div class="form-group">
                <label for="">Product Price</label>
                <input type="number" name="harga">
            </div>
            <div class="form-group">
                <label for="">Product Amount</label>
                <input type="number" name="jumlah">
            </div>
            <div class="form-group">
                <label for="">Product Description</label>
                <input type="text" name="deskripsi">
            </div>
            <div class="form-group">
                <label for="">image</label>
                <input type="file" name="gambar">
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="submit">Submit</button>
            </div>
        </form>
    </div>
</body>

</html>