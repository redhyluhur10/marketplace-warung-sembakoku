<?php 
include 'functions.php';

$product = query("SELECT * FROM produk");
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <script src="fontawesome/all.js"></script>
    <link rel="stylesheet" href="dashboard.css">
</head>
<body>
    <main>
        <input type="checkbox" id="check">
        <label for="check">
            <i class="fas fa-bars" id="btn"></i>
            <i class="fa fa-arrow-right" id="open"></i>
        </label>
        <div class="sidebar">
            <div class="top">
                Dashboard
            </div>  
            <ul>
                <li><a class="#" href="profile.html"><i class="fa fa-home"></i> Profile</a></li>
                <li><a class="#" href="addproduct.php"><i class="fa fa-shopping-basket"></i> Input Product</a></li>
                <li><a class="#" href="stockproduct.php"><i class="fa fa-shopping-bag"></i> Stock Products</a></li>
                <li><a class="#" href="index.html"><i class="fa fa-user-circle"></i> LogOut</a></li>
            </ul>
        </div>
    </main>
    </div>
    <div>
        <center>
        <table border="1">
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Price</th>
                <th>Amount</th>
                <th>Description</th>
                <th>Image</th>
                <th>Action</th>
            </tr>
            <?php $i = 1; ?>
            <?php foreach($product as $row) : ?>
            <tr>
                <td><?= $i ?></td>
                <td><?=$row['nama']; ?></td>
                <td><?=$row['harga']; ?></td>
                <td><?=$row['jumlah'];?></td>
                <td><?=$row['deskripsi']; ?></td>
                <td><img src="<?=$row['gambar']; ?>" </td>
                <td>
                    <a href="ubah.php?id=<?=$row["id"];?>">update</a>
                    <a href="delete.php?id=<?=$row["id"];?>"> Delete</a>
                </td>
            </tr>
            <?php $i++; ?>
            <?php endforeach;?>
        </table>
        </center>
    </div>
</body>
</html>