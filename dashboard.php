<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Dashboard admin</title>
	<script src="fontawesome/all.js"></script>
    <link rel="stylesheet" href="dashboard.css">
</head>

<body>
<main>
        <input type="checkbox" id="check">
        <label for="check">
            <i class="fas fa-bars" id="btn"></i>
            <i class="fa fa-arrow-right" id="open"></i>
        </label>
        <div class="sidebar">
            <div class="top">
                Dashboard
            </div>  
            <ul>
                <li><a class="#" href="profile.html"><i class="fa fa-home"></i> Profile</a></li>
                <li><a class="#" href="addproduct.php"><i class="fa fa-shopping-basket"></i> Input Product</a></li>
                <li><a class="#" href="stockproduct.php"><i class="fa fa-shopping-bag"></i> Stock Products</a></li>
                <li><a class="#" href="index.html"><i class="fa fa-user-circle"></i> LogOut</a></li>
            </ul>
        </div>
    </main>
</body>
</html>