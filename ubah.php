<?php
include "functions.php";
//query data by id
$id = $_GET["id"];
$products = query("SELECT * FROM produk WHERE id = $id")[0];

if (isset($_POST["submit"])){
 
     //query insert data
     $ids = $_GET["id"];
     $nama = $_POST["nama"];
     $harga = $_POST["harga"];
     $jumlah =$_POST["jumlah"];
     $deskripsi =$_POST["deskripsi"];
     $gambar =$_POST["gambar"];

     $query = "UPDATE produk SET 
                 nama = '$nama',
                 harga = '$harga',
                 jumlah = '$jumlah',
                 deskripsi ='$deskripsi',
                 gambar = '$gambar'
                WHERE id = $ids";
    mysqli_query($conn,$query);

    //cek error
    if(mysqli_affected_rows($conn) > 0 ){
        echo"
        <script>
        alert('Update Success')
        document.location.href = 'stockproduct.php'
        </script>
        ";
    }else{
        echo "<script>
        alert('Update failed')
        </script>";
    }

}
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Data Product</title>
    <link rel="stylesheet" href="addproduct.css">
    <link rel="stylesheet" href="dashboard.css">
</head>

<body>
    <main>
        <input type="checkbox" id="check">
        <label for="check">
            <i class="fas fa-bars" id="btn"></i>
            <i class="fa fa-arrow-right" id="open"></i>
        </label>
        <div class="sidebar">
            <div class="top">
                Dashboard
            </div>  
            <ul>
                <li><a class="#" href="#"><i class="fa fa-home"></i> Profile</a></li>
                <li><a class="#" href="addproduct.php"><i class="fa fa-shopping-basket"></i> Input Product</a></li>
                <li><a class="#" href="stockproduct.php"><i class="fa fa-shopping-bag"></i> Stock Products</a></li>
                <li><a class="#" href="logout.php"><i class="fa fa-user-circle"></i> LogOut</a></li>
            </ul>
        </div>
    </main>
    <div class="wrapper">
        <h2>Edit Data Product</h2>
        <input type="hidden" name="id" value="<?= $products["id"]?>">
        <form action="" method="post">
            <div class="form-group">
                <label for="">Product Name</label>
                <input type="text" name="nama" value="<?= $products["nama"] ?>">
            </div>
            <div class="form-group">
                <label for="">Product Price</label>
                <input type="number" name="harga" value="<?= $products["harga"] ?>">
            </div>
            <div class="form-group">
                <label for="">Product Amount</label>
                <input type="number" name="jumlah" value="<?= $products["jumlah"] ?>">
            </div>
            <div class="form-group">
                <label for="">Product Description</label>
                <textarea type="text" name="deskripsi" rows="5" placeholder="Type Description Product here..."
                value="<?= $products["deskripsi"] ?>"></textarea>
            </div>
            <div class="form-group">
                <label for="">image</label>
                <input type="text" name="gambar" value="<?= $products["gambar"] ?>">
            </div>

            <div class="form-group">
                <button type="submit" name="submit" class="submit">Submit</button>
            </div>
        </form>


    </div>
</body>

</html>